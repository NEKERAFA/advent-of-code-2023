extends Node
## Utilities to get day algorithms to process solution

const ALGORITHMS_PATH = "res://scripts/algorithms"

var _algorithms: Dictionary = {}

func _init():
	var regex = RegEx.new()
	regex.compile(r"day_(\d+)(_2)?\.gd")
	
	var dir = DirAccess.open(ALGORITHMS_PATH)
	if not dir:
		push_error("cannot open {0}: {1}".format([ ALGORITHMS_PATH, DirAccess.get_open_error() ]))
		return
		
	dir.include_navigational = false
	dir.list_dir_begin()
	var file = dir.get_next()
	while not file.is_empty():
		if not dir.current_is_dir() && file.ends_with(".gd"):
			var matches = regex.search(file)
			if matches:
				var script = load(ALGORITHMS_PATH.path_join(file))
				if script:
					_algorithms["Day {0}{1}".format([
						matches.get_string(1),
						"" if matches.get_string(2).is_empty() else " - Part Two"
					])] = script.new() as Algorithm
		file = dir.get_next()


## Get the name of algorithms
func get_algorithm_names() -> Array:
	var names = _algorithms.keys()
	names.sort()
	return names

## Get an algorithm object
func get_algorithm(algorithm_name: String) -> Algorithm:
	return _algorithms[algorithm_name]
