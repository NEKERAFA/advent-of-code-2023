extends Window


func _on_accept_pressed():
	hide()


func _on_close_requested():
	hide()


func _on_rich_text_label_meta_clicked(meta):
	OS.shell_open(str(meta))
