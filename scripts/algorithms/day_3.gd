extends Algorithm
## Implementation of Day 3
##
## You and the Elf eventually reach a gondola lift station; he says the gondola lift will take you up to the water source, but this is as far as he can bring you. You go inside.
## [br][br]
## It doesn't take long to find the gondolas, but there seems to be a problem: they're not moving.
## [br][br]
## "Aaah!"
## [br][br]
## You turn around to see a slightly-greasy Elf with a wrench and a look of surprise. "Sorry, I wasn't expecting anyone! The gondola lift isn't working right now; it'll still be a while before I can fix it." You offer to help.
## [br][br]
## The engineer explains that an engine part seems to be missing from the engine, but nobody can figure out which one. If you can add up all the part numbers in the engine schematic, it should be easy to work out which part is missing.
## [br][br]
## The engine schematic (your puzzle input) consists of a visual representation of the engine. There are lots of numbers and symbols you don't really understand, but apparently any number adjacent to a symbol, even diagonally, is a "part number" and should be included in your sum. (Periods (.) do not count as a symbol.)
## [br][br]
## Here is an example engine schematic:
## [codeblock]
## 467..114..
## ...*......
## ..35..633.
## ......#...
## 617*......
## .....+.58.
## ..592.....
## ......755.
## ...$.*....
## .664.598..
## [/codeblock]
## In this schematic, two numbers are not part numbers because they are not adjacent to a symbol: 114 (top right) and 58 (middle right). Every other number is adjacent to a symbol and so is a part number; their sum is 4361.

var _regex

func _init():
	_regex = RegEx.new()
	_regex.compile(r"[^A-Za-z0-9\.]")

## Process the input and returns the output
func process(input: PackedStringArray) -> PackedStringArray:
	var part_numbers: Array[int] = []
	var output: PackedStringArray = []
	
	for lineno in range(input.size()):
		var line = input[lineno]
		var current_number = ""
		var is_adjacent = false
		
		for charno in range(line.length()):
			var character = line[charno]
			if character.is_valid_int():
				current_number += character
				if _is_adjacent_symbol(input, lineno, charno):
					is_adjacent = true
			elif not current_number.is_empty():
				output.append("{0}, is_adjacent: {1}".format([ current_number, is_adjacent ]))
				if is_adjacent:
					part_numbers.append(current_number.to_int())
				current_number = ""
				is_adjacent = false
		
		if not current_number.is_empty():
			output.append("{0}, is_adjacent: {1}".format([ current_number, is_adjacent ]))
			if is_adjacent:
				part_numbers.append(current_number.to_int())
			current_number = ""
			is_adjacent = false

	var result = part_numbers.reduce(func(acc, value): return acc + value)
	output.append("")
	output.append("result: %d" % result)
	return output


# Check if cotains adjacent symbol
func _is_adjacent_symbol(input: PackedStringArray, line: int, character: int) -> bool:
	if line > 0 and character > 0 and _regex.search(input[line - 1][character - 1]):
		return true
	if line > 0 and _regex.search(input[line - 1][character]):
		return true
	if line > 0 and character < input[0].length() - 1 and _regex.search(input[line - 1][character + 1]):
		return true
	if character > 0 and _regex.search(input[line][character - 1]):
		return true
	if character < input[0].length() - 1 and _regex.search(input[line][character + 1]):
		return true
	if line < input.size() - 1 and character > 0 and _regex.search(input[line + 1][character - 1]):
		return true
	if line < input.size() - 1 and _regex.search(input[line + 1][character]):
		return true
	if line < input.size() - 1 and character < input[0].length() - 1 and _regex.search(input[line + 1][character + 1]):
		return true
	
	return false
