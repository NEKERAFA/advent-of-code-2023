extends Algorithm

class Block:
	var src: int
	var dest: int
	var length: int
	
	func _init(src: int, dest: int, length: int):
		self.src = src
		self.dest = dest
		self.length = length

	func has_value(value: int):
		return value >= src and value < src + length

	func convert_value(value: int) -> int:
		return value - src + dest

class SymbolicDict:
	var blocks: Array[Block] = []

	func append_block(src: int, dest: int, length: int):
		blocks.append(Block.new(src, dest, length))

	func convert_value(value: int):
		for i_block in range(blocks.size() - 1, -1, -1): # iterates reverse to prevent redefinitions
			var block = blocks[i_block]
			if block.has_value(value):
				return block.convert_value(value)
	
		return value

## Process the input and returns the output
func process(input: PackedStringArray) -> PackedStringArray:
	var output: PackedStringArray = []
	
#region RegEx definitions
	var seeds_regex = RegEx.create_from_string(r"seeds: (?<seeds>[\d\s]+)")
	var map_header_regex = RegEx.create_from_string(r"(?<map>[\w\-]+) map:")
	var map_regex = RegEx.create_from_string(r"(?<dest_start>\d+) (?<src_start>\d+) (?<length>\d+)")
#endregion
	
	var seeds = []
	var maps: Dictionary = {}
	var current_map: SymbolicDict
	for line in input:
#region Parse seeds
		var result = seeds_regex.search(line)
		if result:
			print("process seeds: %s" % [result.get_string("seeds")])
			var seed_parsed = result.get_string("seeds").split(" ")
			for seed_id in seed_parsed:
				seeds.append(seed_id.to_int())
			continue
#endregion

#region Parse map header
		result = map_header_regex.search(line)
		if result:
			print("parse map header: %s" % result.get_string("map"))
			current_map = SymbolicDict.new()
			maps[result.get_string("map")] = current_map
			continue
#endregion

#region Parse map line
		result = map_regex.search(line)
		if result:
			print("parse map: %s %s %s" % [result.get_string("dest_start"), result.get_string("src_start"), result.get_string("length")])
			var dest = result.get_string("dest_start").to_int()
			var src = result.get_string("src_start").to_int()
			var length = result.get_string("length").to_int()
			current_map.append_block(src, dest, length)
#endregion

	var result = -1
#region Get lowest location
	for seed_id in seeds:
		var value = maps["seed-to-soil"].convert_value(seed_id)
		var output_line = "seed %d -> soil %d -> fertilizer " % [ seed_id, value ]
		value = maps["soil-to-fertilizer"].convert_value(value)
		output_line = "%s%d -> water " % [ output_line, value ]
		value = maps["fertilizer-to-water"].convert_value(value)
		output_line = "%s%d -> light " % [ output_line, value ]
		value = maps["water-to-light"].convert_value(value)
		output_line = "%s%d -> temperature " % [ output_line, value ]
		value = maps["light-to-temperature"].convert_value(value)
		output_line = "%s%d -> humidity " % [ output_line, value ]
		value = maps["temperature-to-humidity"].convert_value(value)
		output_line = "%s%d -> location " % [ output_line, value ]
		value = maps["humidity-to-location"].convert_value(value)
		output.append("%s%d" % [ output_line, value ])
		
		if value < result or result == -1:
			result = value
#endregion 

	output.append("")
	output.append("result: %d" % [result])
	return output
