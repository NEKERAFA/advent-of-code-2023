extends Algorithm

## Process the input and returns the output
func process(input: PackedStringArray) -> PackedStringArray:
	var output: PackedStringArray = []
	var result = 0
	var regex = RegEx.create_from_string(r"Card \s*(?<id>\d+): (?<winning_numbers>[\d\s]+) \| (?<numbers>[\d\s]+)")
	
	for line in input:
		var matches = regex.search(line)
		var id = matches.get_string("id")
		var winning_numbers = Array(matches.get_string("winning_numbers").split(" ", false)).map(func(number): return number.to_int())
		var numbers = Array(matches.get_string("numbers").split(" ", false)).map(func(number): return number.to_int())
		var output_line = "Card %s: " % [ id ]
		var points = 0
		for winning_number in winning_numbers:
			if numbers.has(winning_number):
				output_line = "%s%s%d" % [ output_line, "" if points == 0 else ", ", winning_number ]
				points = (points + 1) if points == 0 else (points * 2)
		output_line = "%s%s = %d" % [ output_line, "None" if points == 0 else "", points ]
		output.append(output_line)
		result += points

	output.append("")
	output.append("result: %d" % [result])
	return output
