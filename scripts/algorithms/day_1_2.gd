extends Algorithm
## Implementation of Day 1 - Part Two
##
## Your calculation isn't quite right. It looks like some of the digits are actually spelled out with letters: [code]one[/code], [code]two[/code], [code]three[/code], [code]four[/code], [code]five[/code], [code]six[/code], [code]seven[/code], [code]eight[/code], and [code]nine[/code] also count as valid "digits". [br][br]
## Equipped with this new information, you now need to find the real first and last digit on each line. For example:
## [codeblock]
## two1nine
## eightwothree
## abcone2threexyz
## xtwone3four
## 4nineeightseven2
## zoneight234
## 7pqrstsixteen
## [/codeblock]
## In this example, the calibration values are [code]29[/code], [code]83[/code], [code]13[/code], [code]24[/code], [code]42[/code], [code]14[/code], and [code]76[/code]. Adding these together produces [code]281[/code].

const VALID_DIGITS = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]

## Process the input and returns the output
func process(input: PackedStringArray) -> PackedStringArray:
	if input.size() == 0:
		push_warning("input is empty")
		return []
	
	var output: PackedStringArray = PackedStringArray()
	
	for lineno in range(input.size()):
		var line = input[lineno]
		var first_digit = null
		var last_digit = null

		var charno = 0
		while charno < line.length():
			var character = line[charno]

			if character.is_valid_int():
				if first_digit == null:
					first_digit = character
				else:
					last_digit = character
			else:
				var digit_length = 1
				var digits = _find_all(line.substr(charno, digit_length))
				while digits.size() > 0 and digit_length < VALID_DIGITS[digits[0]].length():
					digit_length += 1

					if charno + digit_length > line.length():
						digits = []
					else:
						digits = _find_all(line.substr(charno, digit_length))

				if digits.size() == 1:
					if first_digit == null:
						first_digit = digits[0] + 1
					else:
						last_digit = digits[0] + 1

			charno += 1

		if first_digit == null:
			push_error("error on line %d, numbers not found" % lineno)
		else:
			output.append("%s%s" % [ first_digit, last_digit if last_digit != null else first_digit ])
	
	output.append("")
	var result = 0
	for number: String in output:
		result += number.to_int()
	output.append("Result: %d" % result)
	
	return output

func _find_all(text: String) -> Array[int]:
	var found_digits: Array[int] = []

	for pos in range(VALID_DIGITS.size()):
		var digit: String = VALID_DIGITS[pos]
		if digit.begins_with(text):
			found_digits.append(pos)

	return found_digits
