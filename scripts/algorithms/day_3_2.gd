extends Algorithm

## Process the input and returns the output
func process(input: PackedStringArray) -> PackedStringArray:
	var output: PackedStringArray = []
	var potential_gears = _get_potential_gears(input)
	var result = 0
	for key in potential_gears:
		var potential_gear = potential_gears[key]
		if potential_gear.size() == 2:
			var gear_ratio = potential_gear.front() * potential_gear.back()
			output.append("gear: %d * %d = %d" % [ potential_gear.front(), potential_gear.back(), gear_ratio ])
			result += gear_ratio
	
	output.append("")
	output.append("result: %d" % [ result ])
	return output


# Get potential gears
func _get_potential_gears(input: PackedStringArray) -> Dictionary:
	var potential_gears: Dictionary = {}

	for lineno in range(input.size()):
		var line = input[lineno]
		var current_number = ""
		var adjacent_gears: Array[Dictionary] = []
		
		for charno in range(line.length()):
			var character = line[charno]
			if character.is_valid_int():
				current_number += character
				_append_unique_gears(adjacent_gears, _get_adjacent_gears(input, lineno, charno))
			elif not current_number.is_empty():
				if not adjacent_gears.is_empty():
					for adjacent_gear in adjacent_gears:
						var potential_gear_list = potential_gears.get(adjacent_gear, [])
						potential_gear_list.append(current_number.to_int())
						potential_gears[adjacent_gear] = potential_gear_list
				current_number = ""
				adjacent_gears = []

		if not current_number.is_empty():
			if not adjacent_gears.is_empty():
				for adjacent_gear in adjacent_gears:
					var potential_gear_list = potential_gears.get(adjacent_gear, [])
					potential_gear_list.append(current_number.to_int())
					potential_gears[adjacent_gear] = potential_gear_list
			current_number = ""
			adjacent_gears = []

	return potential_gears


# Get the list of adjacent gears
func _get_adjacent_gears(input: PackedStringArray, line: int, character: int) -> Array[Dictionary]:
	var gears: Array[Dictionary] = []
	
	if line > 0 and character > 0 and input[line - 1][character - 1] == "*":
		gears.append({ "line": line - 1, "position": character - 1 })
	if line > 0 and input[line - 1][character] == "*":
		gears.append({ "line": line - 1, "position": character })
	if line > 0 and character < input[0].length() - 1 and input[line - 1][character + 1] == "*":
		gears.append({ "line": line - 1, "position": character + 1 })
	if character > 0 and input[line][character - 1] == "*":
		gears.append({ "line": line, "position": character - 1 })
	if character < input[0].length() - 1 and input[line][character + 1] == "*":
		gears.append({ "line": line, "position": character + 1 })
	if line < input.size() - 1 and character > 0 and input[line + 1][character - 1] == "*":
		gears.append({ "line": line + 1, "position": character - 1 })
	if line < input.size() - 1 and input[line + 1][character] == "*":
		gears.append({ "line": line + 1, "position": character })
	if line < input.size() - 1 and character < input[0].length() - 1 and input[line + 1][character + 1] == "*":
		gears.append({ "line": line + 1, "position": character + 1 })
	
	return gears


# Append unique gears values
func _append_unique_gears(gears: Array[Dictionary], others: Array[Dictionary]):
	for gear in others:
		if not gears.any(_compare_gear(gear)):
			gears.append(gear)

func _compare_gear(gear):
	return func(other):
		return gear["line"] == other["line"] and gear["position"] == other["position"]
