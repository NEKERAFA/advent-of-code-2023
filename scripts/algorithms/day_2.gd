extends Algorithm
## Implementation of Day 2
##
## You're launched high into the atmosphere! The apex of your trajectory just barely reaches the surface of a large island floating in the sky. You gently land in a fluffy pile of leaves. It's quite cold, but you don't see much snow. An Elf runs over to greet you.
## [br][br]
## The Elf explains that you've arrived at Snow Island and apologizes for the lack of snow. He'll be happy to explain the situation, but it's a bit of a walk, so you have some time. They don't get many visitors up here; would you like to play a game in the meantime?
## [br][br]
## As you walk, the Elf shows you a small bag and some cubes which are either red, green, or blue. Each time you play this game, he will hide a secret number of cubes of each color in the bag, and your goal is to figure out information about the number of cubes.
## To get information, once a bag has been loaded with cubes, the Elf will reach into the bag, grab a handful of random cubes, show them to you, and then put them back in the bag. He'll do this a few times per game.
## [br][br]
## You play several games and record the information from each game (your puzzle input). Each game is listed with its ID number (like the 11 in Game 11: ...) followed by a semicolon-separated list of subsets of cubes that were revealed from the bag (like 3 red, 5 green, 4 blue).
## [br][br]
## For example, the record of a few games might look like this:
## [codeblock]
## Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
## Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
## Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
## Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
## Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
## [/codeblock]
## In game 1, three sets of cubes are revealed from the bag (and then put back again). The first set is 3 blue cubes and 4 red cubes; the second set is 1 red cube, 2 green cubes, and 6 blue cubes; the third set is only 2 green cubes.
## [br][br]
## The Elf would first like to know which games would have been possible if the bag contained only 12 red cubes, 13 green cubes, and 14 blue cubes?
## [br][br]
## In the example above, games 1, 2, and 5 would have been possible if the bag had been loaded with that configuration. However, game 3 would have been impossible because at one point the Elf showed you 20 red cubes at once; similarly, game 4 would also have been impossible because the Elf showed you 15 blue cubes at once. If you add up the IDs of the games that would have been possible, you get 8.

## Process the input and returns the output
func process(input: PackedStringArray) -> PackedStringArray:
	var ids: Array[int] = []
	var output: PackedStringArray = []
	
	var regex = RegEx.new()
	regex.compile(r"Game (?<id>\d+): (?<cubes>.+)")
	
	for lineno in range(input.size()):
		var line = input[lineno]
		var result := regex.search(line)
		if result == null:
			output.append("cannot parse line %d" % lineno)
		else:
			var id = result.get_string("id").to_int()
			var content = "id: %d, " % id
			var cubes = result.get_string("cubes")
			var subsets = cubes.split("; ")

			var max_red = 0
			var max_green = 0
			var max_blue = 0

			for subset in subsets:
				var cubes_showeds = subset.split(", ")
				for cubes_count in cubes_showeds:
					if cubes_count.ends_with("red"):
						max_red = max(max_red, cubes_count.to_int())
					elif cubes_count.ends_with("green"):
						max_green = max(max_green, cubes_count.to_int())
					elif cubes_count.ends_with("blue"):
						max_blue = max(max_blue, cubes_count.to_int())

			content += "max red: %d, max green: %d, max blue: %d" % [max_red, max_green, max_blue]
			output.append(content)
			
			if max_red <= 12 and max_green <= 13 and max_blue <= 14:
				output.append("posible")
				ids.append(id)
			else:
				output.append("imposible")

	output.append("")
	output.append("result: %d" % ids.reduce(func(acc, id): return acc + id))
	return output
