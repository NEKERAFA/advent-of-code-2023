extends Algorithm
## Implementation of Day 2 - Part Two
##
## The Elf says they've stopped producing snow because they aren't getting any water! He isn't sure why the water stopped; however, he can show you how to get to the water source to check it out for yourself. It's just up ahead!
## [br][br]
## As you continue your walk, the Elf poses a second question: in each game you played, what is the fewest number of cubes of each color that could have been in the bag to make the game possible?
## [br][br]
## Again consider the example games from earlier:
## [codeblock]
## Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
## Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
## Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
## Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
## Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
## [/codeblock]
## - In game 1, the game could have been played with as few as 4 red, 2 green, and 6 blue cubes. If any color had even one fewer cube, the game would have been impossible.[br]
## - Game 2 could have been played with a minimum of 1 red, 3 green, and 4 blue cubes.[br]
## - Game 3 must have been played with at least 20 red, 13 green, and 6 blue cubes.[br]
## - Game 4 required at least 14 red, 3 green, and 15 blue cubes.[br]
## - Game 5 needed no fewer than 6 red, 3 green, and 2 blue cubes in the bag.[br]
## [br]
## The power of a set of cubes is equal to the numbers of red, green, and blue cubes multiplied together. The power of the minimum set of cubes in game 1 is 48. In games 2-5 it was 12, 1560, 630, and 36, respectively. Adding up these five powers produces the sum 2286.

## Process the input and returns the output
func process(input: PackedStringArray) -> PackedStringArray:
	var powers_sets: Array[int] = []
	var output: PackedStringArray = []
	
	var regex = RegEx.new()
	regex.compile(r"Game (?<id>\d+): (?<cubes>.+)")
	
	for lineno in range(input.size()):
		var line = input[lineno]
		var result := regex.search(line)
		if result == null:
			output.append("cannot parse line %d" % lineno)
		else:
			var id = result.get_string("id").to_int()
			var content = "id: %d, " % id
			var cubes = result.get_string("cubes")
			var subsets = cubes.split("; ")

			var max_red = 0
			var max_green = 0
			var max_blue = 0

			for subset in subsets:
				var cubes_showeds = subset.split(", ")
				for cubes_count in cubes_showeds:
					if cubes_count.ends_with("red"):
						max_red = max(max_red, cubes_count.to_int())
					elif cubes_count.ends_with("green"):
						max_green = max(max_green, cubes_count.to_int())
					elif cubes_count.ends_with("blue"):
						max_blue = max(max_blue, cubes_count.to_int())

			content += "max red: %d, max green: %d, max blue: %d" % [max_red, max_green, max_blue]
			output.append(content)
			
			var power_set = max_red * max_green * max_blue
			output.append("Power of minimun set: %d" % power_set)
			powers_sets.append(power_set)

	output.append("")
	output.append("result: %d" % powers_sets.reduce(func(acc, power_set): return acc + power_set))
	return output
