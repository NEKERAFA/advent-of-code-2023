extends Algorithm

## Process the input and returns the output
func process(input: PackedStringArray) -> PackedStringArray:
	var output: PackedStringArray = []
	var regex = RegEx.create_from_string(r"Card \s*(?<id>\d+): (?<winning_numbers>[\d\s]+) \| (?<numbers>[\d\s]+)")
	
	var instances = {}
	for line in input:
		var matches = regex.search(line)

#region Add one copy of instance
		var id = matches.get_string("id").to_int()
		instances[id] = instances.get(id, 0) + 1
		print("processing %d card, %d times" % [id, instances[id]])
		#var output_line = "Card %d = " % [id]
#endregion

#region Get matching numbers
		var winning_numbers = Array(matches.get_string("winning_numbers").split(" ", false)).map(func(number): return number.to_int())
		var numbers = Array(matches.get_string("numbers").split(" ", false)).map(func(number): return number.to_int())
		var matching_numbers = 0
		for winning_number in winning_numbers:
			if numbers.has(winning_number):
				matching_numbers += 1
#endregion

		if matching_numbers > 0:
#region Add winned copies
			for times in range(instances[id]):
				for new_card in range(id + 1, id + matching_numbers + 1):
					#output_line += "%scard %d" % ["" if new_card == id + 1 and times == 0 else ", ", new_card]
					instances[new_card] = instances.get(new_card, 0) + 1
		#else:
			#output_line += "None"
#endregion
		#output.append(output_line)

	#output.append("-------------")
#region Count total copies
	var result = 0
	for id in instances:
		output.append("Card %d = %d" % [id, instances[id]])
		result += instances[id]
#endregion

	output.append("")
	output.append("result: %d" % result)
	return output
