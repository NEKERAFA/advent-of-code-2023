extends MarginContainer

@export var sound_button : Button
@export var show_help : Button
@export var input_content : TextEdit
@export var output_content : TextEdit
@export var algorithm_selector : OptionButton

func _ready() -> void:
	for algorithm_name in AlgorithmsLoader.get_algorithm_names():
		algorithm_selector.add_item(algorithm_name)


func _on_run_pressed():
	if input_content.text.strip_edges().is_empty():
		output_content.text = "Input is empty!"
		input_content.grab_focus()
		return
	
	var id = algorithm_selector.get_selected_id()
	var algorithm_name = algorithm_selector.get_item_text(id)
	var algorithm := AlgorithmsLoader.get_algorithm(algorithm_name)
	if algorithm != null:
		var result := algorithm.process(input_content.text.strip_edges().split("\n"))
		output_content.text = "\n".join(result)


func _on_reset_pressed():
	output_content.text = ""


func _on_info_pressed():
	$About.show()


func _on_volume_pressed():
	if $AudioStreamPlayer.playing:
		sound_button.text = "volume_up"
	else:
		sound_button.text = "volume_off"

	$AudioStreamPlayer.stream_paused = !$AudioStreamPlayer.stream_paused
